"""
le dossier dans le quel le script doit etre éxécuter require la disponibilité des fichier :
	fichier de sortie de Viridic (pour la premiere étape et la derniere étape aussi)
	fichier du mapping des Reads sur les contigs dans chaque virome (pour la derniere étape)

Des fichier de teste sur petite jeux de données pour les partie 1 et 3 seront en piéce jointe, pour tester le script 
j'ai construis des fichiers de teste car les jeux de doonées sont trop volumineux, et aussi ilmanque un fichier pour 
la derniere partie que je n'ai pas recus)

"""
# *****Les résultats des parties 1 et 2(a l'exception du dendrogramme qui sera afficher pour la partie 2) seront redériger 
		#directement dans des fichiers.txt qui seront réutiliser en meme temp aussi entre les scripts et seront disponible, 
		#dans le répertoire déxécution du fichier.py.Et la partie 3 donne 3 fichier de sortie ( meme chose seront disponible
												#dans le mem répértoire) 
#Dans Pycharm je suis obligé de fermer le dendrogramme pour que la suite du script soit faite et donc ce qui permet de géneré les derneire fichier
				#donc peut etre vous pouvez avoir la meme chose


import sys
sys.setrecursionlimit(10000)
from scipy.cluster.hierarchy import ward, fcluster
from scipy.cluster.hierarchy import dendrogram, linkage
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
labels = set()
#############################################################  1eme partie ---------------------------------------------

# Ouverture d'un fichier en mode Write, les sortie standar de ce premeire scripts seront redéreger vers un fichier "FichierPourClustering" qui sera directemnt utilsier dans le clustering
with open("FichierPoureClustring.txt", "w") as external_fileA :
    # le fichier qui sera ouvert dans cette étape est celui récuepre des sortie de viridic ( le nom est a changé en blast_sim_DF.txt)
    # ce fichier est crée pour tester uniquement un petit jeux de données.
    with open("blast_sim_DF.txt", "r") as f1:
        for li in f1:
            li = li.rstrip("\n")
            li = re.sub(r'[^\w\s.]', '', li).split("\t")
            if li[0][1].isupper():
                if float(li[11]) > 0.8 and float(li[12]) > 0.8:
                    if (float(li[4]) / (float(li[5])) > 0.95) and (
                            (float(li[6]) / ((float(li[12]) * float(li[3])) / 1)) > 0.95):
                        print(li[0],"-",li[1],"\t\t",li[9],file = external_fileA)
external_fileA.close()

#############################################################  2eme partie ---------------------------------------------
#Ouverture d'un fichier en mode Write, ce fichier sera utilser dans la derniere partie du projet
with open("LefichierDesGrpdeClusters.txt","w") as external_fileB:
    #A partir des fichiers de sortie de létape précedente
    with open("FichierPoureClustring.txt","r") as f1:
        for ligne in f1:
            ligne = ligne.strip("\n").split("\t")
            colonne = ligne[0].split(" - ")
            labels.add(colonne[0].strip())
            labels.add(colonne[1].strip())

    #Conversion des donnée du fichier comprenant les distance en DataFrame
    mydata = pd.read_table("FichierPoureClustring.txt",sep='\t\t',header=None, engine='python')
    mydata.dropna(axis=1, how='all', inplace=True)#mydata is representing the data file

    # Création d'une nouvelle DataFrame, dans le but de construire a la fin une matrice carré
    data = []
    df = pd.DataFrame(data, columns=labels, index=labels) # df est la matrice vide qu'on cherche à construire

                #Derniere etape :remplire la matrice vide : df
    for index, row in mydata.iterrows(): # index = idexe de ligne ; row = valeur du croisement ligne colonne
        distance = row[1]
        labels = str(row[0]).strip().split(" - ")
        df[labels[0]][labels[1]] = distance
        df[labels[1]][labels[0]] = distance


    # ****A Construction de la matrice a utiliser prochainement pour le clustering
    df = df.fillna(100) #rempalcer les valeur NaN avec la valeur 100 ( probléme fichier ne contien pas toutes les possibilités de comparaison, certin contigs sont comparis uniquement avec 2 ou 3 contigs)
    vals = df.values    # transformer la data frame remplie en array


    # ****B ploting de du dendrogramme ==========> Rappelle, le plot j'ai mis le plot dans une variable a, car si le ploting n'est pas dnas une variable il vas interrompre
    Z = linkage(vals, 'ward',metric='euclidean')  # le renvoie de la sortie standar dans le fichier souhaiter, pour cela je mis le ploting dans une varibale et il sera afficher et la sortie sera aussi bien faite dans le fichier souhaiter
    fig = plt.figure(figsize=(10, 5))
    dn = dendrogram(Z,labels = df.index,show_leaf_counts=True)
    plt.title("Dendrograms")
    plt.xlabel('Les Contigs')
    plt.ylabel('Distance')
    #plt.axhline(y=60, color='r', linestyle='--') #pour limité les groupe par une ligne
    a=plt.show()

    # ****C Récupération des groupes de contigs (populations) ===>>> ici la distance est a 60 mais c'est a changé !!!!
                        # former les groupe a appartir de la distance = ici il prend 8), avec attribution des indices pour les contigs appartenant aux meme groupes donc meme indice
    groupes_cah = fcluster(Z,t=8,criterion='distance')
    #print("Attribution des groupes",groupes_cah)# les groupe de clusters , l'affichage des indice, vas dépendre de l'affichage du dendrogramme (il change, mais l'inforamtions reste la meme)
                      #Groupements des contigs en population, on utilisant les indice deja attribuer avant..
    idg = np.argsort(groupes_cah)
    #print("Contigs               indice:",pd.DataFrame(groupes_cah[idg],df.index[idg]))


    # ****D Récupération des clusters dans des liste :
    #lidée est de crée a partir de ce fichier une colonne qui prend les num de groupe ,et appartir de ces numéro
    #je construirait mes liste de groupe
    Lescluster = pd.DataFrame(groupes_cah[idg],df.index[idg])
    dict_groupes = {}
    chiffre = set()
    myClusters = []
    for index, row in Lescluster.iterrows(): # index ici  =  les genome
        dict_groupes[index] = str(row.values)
        chiffre.add(str(row.values))
    for nombre in chiffre:
        cluster = list()
        for cle, val in dict_groupes.items():
            if nombre == val:
                cluster.append(cle)
        myClusters.append(cluster)
    #Impression des populations de contigs
    for i in myClusters:
        print(i,"\n",file=external_fileB)  # la sortie sera rediriger vers le mapping ( les groupe de contigs seront utiliser ...)

external_fileB.close()

import sys
import pandas as pd
import re
#############################################################  3eme partie ---------------------------------------------

dicGroupes= {}
# Pour la question 3 :  RPKM
dictLenContigs = {} # Construction d'un dictionnaire , en keys : contigs , values : Longeur du contig , appartir du fichier de sortei num 1 de VIRIDIC
lesContigs = []

#lire deux fichier pour :construire un dict de contigs et leur longeur, et les groupes de contigs = populations
with open("LefichierDesGrpdeClusters.txt","r") as f1,open("fichierVIRIDIC Q3partie3.txt","r") as f2:
    for lIgne in f2:
        lIgne =re.sub(r'[^\w\s.]', '',lIgne.strip("\n")).split("\t") # je rempalce les ponctuation dans j'ai pas besoin et je split mes ligne
        if lIgne[0][1].isupper():# pour éviter la premire ligne contenat les noms de colonnes
            dictLenContigs[lIgne[0]] =lIgne[2]
            dictLenContigs[lIgne[1]] =lIgne[3]

    for line in f1:
        line = re.sub(r'[^\w.,]','', line.strip("\n").replace(" ","")).split(",")
        if not line == [""]:
            lesContigs.append(line) # les groupes de contigs misent dans une liste ( [[pop1],[pop2],[pop3]]

#dictionnaire des groupes de virus : cle : numéro du groupe, valeur : les contigs du groupe courant
compteur = 0
for groupes in lesContigs:
    compteur+=1
    dicGroupes[compteur] = groupes


# Transfomer le fichier contenat les reads mappés.. a une DataFrame
tableau = pd.read_csv("reads-contigs-virome.txt",sep="\t", index_col=0) # fichier du mapping convertie en dataframe

# Ouverture des fichier en mode écriture, pour renvoyé directement les sorties des print de chaques parties dans les fichiers demandés
with open("population_Mapping_nbHits.tsv", "w") as external_file,open("population_Mapping_cov.tsv", "w") as external_file1,open("population_Mapping_RPKM.tsv","w")as external_file2:
    print("\t\t\t\tNb-Tot-Reds-mapper-Pop-ViromCourant",file = external_file)
    print("\t\t\tCouverture_Moyenne_Pops_Virome",file = external_file1)
    # cl : viromes, vl avec .values : une liste des valeur de cette colonne cl courante, vl =
    for cl, vl in tableau.items():
        print("Virome :",cl,file=external_file)
        print(cl,file=external_file1)#vers le fichier2 de couverture
        print(cl,file=external_file2)#vers le fichier3 de couverture
        RPKM = 0 #Q3
        comptcovertTotParPopul = 0#Q1
        lisTailTotPop = []#Q2
        for numerocontig,grpcontig in dicGroupes.items():  # je suis dans le dictio des Population des contigs
            comptreads = 0#Q1
            taiTotConPop = 0
            for contig in grpcontig:  # itération avec 'contigs' sur toutes les contigs de chaque populations
                if contig in vl.index:
                    comptreads += int(vl.loc[contig]) # récupération des nb de reads pour chaque contigs courant /dans une population / et dans un virome
                comptcovertTotParPopul += int(vl.loc[contig])  # somme des reads mappant cette population de contigs
                taiTotConPop+= int(dictLenContigs[contig])# les tailles des contigs
            print("Population", numerocontig,("\t")*2,comptreads,"\t"*9,file =external_file)
            print("Population", numerocontig,"\t",(comptreads*100)/taiTotConPop,file =external_file1)
            #lisTailTotPop.append(taiTotConPop) # Q2 : la somme des contigs dans chaque populations
        print("Totale reads\t\t",comptcovertTotParPopul,file=external_file)


        #Q3 LES RPKM
        #print("RPKM:(de chaque contigs)")
        # je sépare les deux script
        for numerocontig2,grpcontig2 in dicGroupes.items():
            comptreads2 = 0
            for contiG in grpcontig2:  # itération avec 'contigs' sur toutes les contigs de chaque populations
                #comptreads2 += int(vl.loc[contiG])
                RPKM= (int(vl.loc[contiG]) * 10**3 * 10**6)//(int(comptcovertTotParPopul) * 10**6 * int(dictLenContigs[contiG])) # le // car il  pourrait pas voir un redas et demi par exemple
                print("\tContig:",contiG,"RPKM :",RPKM,file =external_file2)

        print("\n",file=external_file)
        print("\n",file=external_file1)
        print("\n",file=external_file2)
external_file.close()
external_file1.close()
external_file2.close()
